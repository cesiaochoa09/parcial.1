from django.contrib import admin

# Register your models here.
from .models import GrantGoal

@admin.register(GrantGoal)
class GrantGoalAdmin(admin.ModelAdmin):
    list_display = [
        "grantgoal_name",
        "timestamp",
        "final_date",
        "days_duration",
        "status"
    ]
